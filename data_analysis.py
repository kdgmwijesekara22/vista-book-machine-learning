import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sqlalchemy import create_engine, exc
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM

# todo remove
# Load data
# train_data_original = pd.read_csv('data/sales_train.csv')
# test_data = pd.read_csv('data/test.csv')
# submission = pd.read_csv('data/sample_submission.csv')

# MySQL configurations
username = 'root'
password = '1234'
host = 'localhost'
database = 'vista_modal'  # Ensure this database exists in MySQL before running the script

# Establish a connection to the database
engine = create_engine(f"mysql+pymysql://{username}:{password}@{host}/{database}")

# Function to fetch data from MySQL database
def fetch_data(table_name):
    query = f"SELECT * FROM {table_name}"
    df = pd.read_sql(query, engine)
    return df

# Fetch data from database
train_data_original = fetch_data('sales_train')
test_data = fetch_data('test_data')
submission = fetch_data('sample_submission')

# todo remove
# Function to convert a CSV to a table in MySQL database
# Function to convert a CSV to a table in MySQL database
# def csv_to_mysql(df, table_name):
#     # Convert DataFrame to MySQL table
#     df.to_sql(table_name, engine, if_exists='replace', index=False)
#     print(f"Table {table_name} has been created in {database}.")

# todo remove
# Convert sales_train.csv to MySQL table
# csv_to_mysql(train_data_original, 'sales_train')
# csv_to_mysql(test_data, 'test_data')
# csv_to_mysql(submission, 'sample_submission')

# Display initial data
print("Initial Train Data:")
print(train_data_original.head())
print("\nTest Data:")
print(test_data.head())

# Data Exploration

# Extract day, month, and year from the date column
train_data_original['date'] = pd.to_datetime(train_data_original['date'], dayfirst=True)
train_data_original['day'] = train_data_original['date'].dt.day
train_data_original['month'] = train_data_original['date'].dt.month
train_data_original['year'] = train_data_original['date'].dt.year

# Monthly sales trends
plt.figure(figsize=(12, 6))
monthly_sales = train_data_original.groupby(['year', 'month'])['item_cnt_day'].sum()
monthly_sales.plot(title='Monthly Sales Trends')
plt.show()

# Average monthly sales of top 50 items
plt.figure(figsize=(12, 6))
avg_monthly_sales = train_data_original.groupby('item_id')['item_cnt_day'].mean()
avg_monthly_sales.nlargest(50).plot(kind='bar', title='Average Monthly Sales of Top 50 Items')
plt.show()

# Total sales of shops
plt.figure(figsize=(12, 6))
total_shop_sales = train_data_original.groupby('shop_id')['item_cnt_day'].sum()
total_shop_sales.plot(kind='bar', title='Total Sales of Shops')
plt.show()

# Preprocessing for Model

train_Data = train_data_original.pivot_table(index=['shop_id', 'item_id'], values=['item_cnt_day'], columns=['date_block_num'], fill_value=0, aggfunc='sum')
train_Data.columns = [col[0] + '_' + str(col[1]) for col in train_Data.columns]
train_Data.reset_index(inplace=True)

test_Data = test_data.copy()
test_Data = test_Data.pivot_table(index=['shop_id', 'item_id'], fill_value=0).reset_index()

# Merge train and test data
Combine_train_test = pd.merge(test_Data, train_Data, how='left', on=['shop_id', 'item_id']).fillna(0)
Combine_train_test = Combine_train_test.sort_values(by='ID')
Combine_train_test = Combine_train_test.drop(columns=['ID'])

# Prepare data for model
X_train = np.array(Combine_train_test.values[:, :-1]).reshape(Combine_train_test.values[:, :-1].shape[0], Combine_train_test.values[:, :-1].shape[1], 1)
y_train = Combine_train_test.values[:, -1:]
X_test = np.array(Combine_train_test.values[:, 1:]).reshape(Combine_train_test.values[:, 1:].shape[0], Combine_train_test.values[:, 1:].shape[1], 1)

# Model Building
model = Sequential()
model.add(LSTM(50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
model.add(LSTM(50, return_sequences=True))
model.add(LSTM(50))
model.add(Dense(25))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')

# Train the model
history = model.fit(X_train, y_train, epochs=15, batch_size=128)

# Improved plotting of training loss over epochs
plt.figure(figsize=(12, 6))

# Plotting the loss
sns.lineplot(x=np.arange(1, 16), y=history.history['loss'], color='r', marker='o', linestyle='-')

# Title and labels
plt.title("Training Loss over Epochs", fontsize=18)
plt.xlabel("Epoch", fontsize=14)
plt.ylabel("Mean Squared Error (Loss)", fontsize=14)
plt.xticks(np.arange(1, 16, 1))
plt.grid(axis='y')

# Displaying the plot
plt.tight_layout()
plt.legend(title='Training Loss', loc='upper right')
plt.show()

# Plot training loss
plt.figure(figsize=(10, 10))
sns.lineplot(x=np.arange(1, 16), y=history.history['loss'], color='r', label="Training Loss")
plt.title("Training Loss over Epochs")
plt.show()

# Predictions
submission['item_cnt_month'] = model.predict(X_test).clip(0, 30)
submission.to_csv('submission.csv', index=False)
